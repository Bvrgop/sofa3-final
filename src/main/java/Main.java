import controllers.ActivityController;
import controllers.BoardController;
import controllers.ItemController;
import controllers.SprintController;
import domain.*;
import domain.backlog.ItemObservable;
import domain.identity.Developer;
import domain.identity.ProductOwner;
import domain.identity.ScrumMaster;
import domain.repository.SCMType;
import domain.sprint.ExportType;
import domain.sprint.SprintObservable;
import domain.sprint.SprintType;
import infrastructure.ActivityService;
import infrastructure.BoardService;
import infrastructure.ItemService;
import infrastructure.SprintService;

public class Main {

    private BoardService boardService;
    private ItemService itemService;
    private ActivityService activityService;
    private SprintService sprintService;

    private BoardController boardController;
    private ItemController itemController;
    private SprintController sprintController;
    private ActivityController activityController;

    public Main() {
        this.boardService = new BoardService();
        this.itemService = new ItemService();
        this.activityService = new ActivityService();
        this.sprintService = new SprintService();
        this.boardController = new BoardController(this.boardService);
        this.itemController = new ItemController(this.sprintService, this.itemService, this.boardService);
        this.sprintController = new SprintController(this.sprintService, this.itemService, this.boardService);
        this.activityController = new ActivityController(this.activityService);
    }

    public static void main(String[] args) {
        Main main = new Main();
        main.start();
    }

    public void start() {
        Board b1 = this.boardController.createBoard(new ProductOwner("James"), "SOA3", SCMType.GIT);

        CustomObserver o1 = new ScrumMaster("Mike");
        CustomObserver o2 = new Developer("Dan");
        CustomObserver o3 = new Developer("John");

        b1.addUser(o1.getName(), o1);
        b1.addUser(o2.getName(), o2);
        b1.addUser(o3.getName(), o3);

        SprintObservable s1 = this.sprintController.createSprint(b1.getName(), SprintType.RELEASE, "Initial sprint", b1.getUsers().get("Mike"), 7);
        SprintObservable s2 = this.sprintController.createSprint(b1.getName(), SprintType.REVIEW, "Second sprint", b1.getUsers().get("Mike"), 14);

        ItemObservable i1 = this.itemController.createItem("Creation", "Make creation services", "SOA3");
        ItemObservable i2 = this.itemController.createItem("Observable setup", "Make observable patterns", "SOA3");
        ItemObservable i3 = this.itemController.createItem("Adapter", "Make git adapters", "SOA3");
        ItemObservable i4 = this.itemController.createItem("Strategy", "Creation sprint strategies", "SOA3");

        this.activityController.createActivity(i1, "Main Component", "this is the main component of the creation services", b1.getUsers().get("Dan"));
        this.activityController.createActivity(i2, "Secondary factories", "Multiple factories should be set up", b1.getUsers().get("John"));
        this.activityController.createActivity(i2, "Create notification code", "Observers should have notification receiving code", b1.getUsers().get("John"));
        this.activityController.createActivity(i2, "Setup broadcasting code", "Observer broadcasting code should be set up", b1.getUsers().get("Dan"));
        this.activityController.createActivity(i2, "Adapter setup", "Setup connection to git via adapter", b1.getUsers().get("Dan"));
        this.activityController.createActivity(i2, "Strategy Development", "Develop export strategies", b1.getUsers().get("John"));

        this.itemController.moveToSprint(s1.getName(), i1.getName());
        this.itemController.moveToSprint(s1.getName(), i2.getName());
        this.itemController.moveToSprint(s2.getName(), i3.getName());
        this.itemController.moveToSprint(s2.getName(), i4.getName());

        i1.addObserver(o1);
        i1.addObserver(o2);
        i1.addObserver(o3);

        i1.getDiscussionThread().sendMessage(o1, "Test message.");
        for (String s : i1.getDiscussionThread().getMessages()) {
            System.out.println(s);
        }

        s1.export(ExportType.PNG, true);
    }
}
