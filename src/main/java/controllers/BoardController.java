package controllers;

import domain.Board;
import domain.identity.ProductOwner;
import domain.repository.SCMType;
import infrastructure.BoardService;
import java.io.InvalidClassException;

public class BoardController {

    private BoardService boardService;

    public BoardController(BoardService s) {
        this.boardService = s;
    }

    public Board createBoard(ProductOwner owner, String name, SCMType a) {
        try {
            Board b = new Board(owner, name, a);
            this.boardService.getBoards().put(name, b);
            return b;
        } catch (InvalidClassException e) {
            System.out.println(e.getMessage());
            return null;
        }
    }
}
