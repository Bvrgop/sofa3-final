package controllers;

import domain.CustomObserver;
import domain.sprint.SprintObservable;
import domain.sprint.SprintType;
import infrastructure.BoardService;
import infrastructure.ItemService;
import infrastructure.SprintService;

public class SprintController {

    private SprintService sprintService;
    private ItemService itemService;
    private BoardService boardService;

    public SprintController(SprintService sprintService, ItemService itemService, BoardService boardService) {
        this.sprintService = sprintService;
        this.itemService = itemService;
        this.boardService = boardService;
    }

    public SprintObservable createSprint(String boardName, SprintType t, String name, CustomObserver scrummaster, int durationIndays) {
        return this.sprintService.createSprint(this.boardService.getBoards().get(boardName), t, name, scrummaster, durationIndays);
    }
}
