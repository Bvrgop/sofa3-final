package controllers;

import domain.backlog.Activity;
import domain.CustomObserver;
import domain.backlog.ItemObservable;
import infrastructure.ActivityService;

public class ActivityController {

    private ActivityService activityService;

    public ActivityController(ActivityService activityService) {
        this.activityService = activityService;
    }

    public Activity createActivity(ItemObservable i, String name, String description, CustomObserver dev) {
        return activityService.create(i, new Activity(name, description, dev));
    }
}
