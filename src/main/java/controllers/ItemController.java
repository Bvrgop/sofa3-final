package controllers;

import domain.Board;
import domain.backlog.ItemObservable;
import domain.sprint.SprintObservable;
import infrastructure.BoardService;
import infrastructure.ItemService;
import infrastructure.SprintService;

import java.util.HashMap;
import java.util.Map;

public class ItemController {

    private SprintService sprintService;
    private ItemService itemService;
    private BoardService boardService;

    public ItemController(SprintService sprintService, ItemService itemService, BoardService boardService) {
        this.sprintService = sprintService;
        this.itemService = itemService;
        this.boardService = boardService;
    }

    public ItemObservable createItem(String name, String description, String board) {
        Board b = this.boardService.getBoards().get(board);
        ItemObservable i = new ItemObservable(b, name, description);
        itemService.getBacklogItems().put(name, i);
        b.getBacklog().put(name, i);

        return i;
    }

    public void moveToSprint(String sprint, String item) {
        SprintObservable s = this.sprintService.getSprints().get(sprint);
        ItemObservable i = this.itemService.getBacklogItems().get(item);

        if (i == null) {
            System.out.println("No backlog item was found");
        } else {
            if (i.getBoard().equals(s.getBoard())) {
                s.getItems().put(i.getName(), i);
                i.setSprint(s);
            } else {
                System.out.println("Item didn't belong to same board.");
            }
        }
    }

    public void removeFromSprint(String sprint, String item) {
        SprintObservable s = this.sprintService.getSprints().get(sprint);
        ItemObservable i = s.getItems().get(item);

        if (i == null) {
            System.out.println("No backlog item was found");
            return;
        } else {
            s.getItems().remove(item);
            i.setSprint(null);
        }
    }

    public Map<String, ItemObservable> getBacklog() {
        return itemService.getBacklogItems();
    }
}
