package infrastructure;

import domain.backlog.Activity;
import domain.backlog.ItemObservable;

public class ActivityService {

    public Activity create(ItemObservable i, Activity a) {
        i.getActivities().add(a);
        return a;
    }
}
