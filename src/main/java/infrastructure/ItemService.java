package infrastructure;

import domain.backlog.ItemObservable;

import java.util.HashMap;
import java.util.Map;

public class ItemService {

    private HashMap<String, ItemObservable> backlogItems;

    public ItemService() {
        this.backlogItems = new HashMap<>();
    }

    public Map<String, ItemObservable> getBacklogItems() {
        return backlogItems;
    }
}
