package infrastructure;

import domain.Board;

import java.util.HashMap;
import java.util.Map;

public class BoardService {
    private HashMap<String, Board> boards;

    public BoardService() {
        this.boards = new HashMap<>();
    }

    public Map<String, Board> getBoards() {
        return boards;
    }
}
