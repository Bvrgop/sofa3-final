package infrastructure;

import domain.Board;
import domain.CustomObserver;
import domain.sprint.*;

import java.io.InvalidClassException;
import java.util.HashMap;
import java.util.Map;

public class SprintService {
    private HashMap<String, SprintObservable> sprints;
    private SprintStateFactory createdFactory;
    private SprintStateFactory runningFactory;
    private SprintStateFactory finishedFactory;
    private FailedStateFactory failedStateFactory;
    private ClosedStateFactory closedStateFactory;

    public SprintService() {
        this.sprints = new HashMap<>();
        this.createdFactory = new CreatedStateFactory();
        this.runningFactory = new RunningStateFactory();
        this.finishedFactory = new FinishedStateFactory();
        this.failedStateFactory = new FailedStateFactory();
        this.closedStateFactory = new ClosedStateFactory();
    }

    public SprintObservable createSprint(Board b, SprintType t, String name, CustomObserver scrumMaster, int amountOfDays) {
        try {
            SprintObservable s = new SprintObservable(b, scrumMaster, t, createdFactory, runningFactory, finishedFactory, failedStateFactory, closedStateFactory, name, amountOfDays);
            this.sprints.put(name, s);
            b.getSprints().put(name, s);
            return s;
        } catch (InvalidClassException e) {
            System.out.println("User selected was not a scrummaster.");
            return null;
        }
    }

    public Map<String, SprintObservable> getSprints() {
        return sprints;
    }
}
