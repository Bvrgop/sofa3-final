package domain.backlog;

public class ToDoState implements ItemState {
    private ItemObservable itemObservable;
    public ToDoState(ItemObservable itemObservable) {
        this.itemObservable = itemObservable;
    }
    @Override
    public void toDo() {
        System.out.println("This item is already in to do");
    }

    @Override
    public void doing() {
        this.itemObservable.setState(itemObservable.getDoingState());
    }

    @Override
    public void readyForTesting() {
        System.out.println("Can't proceed to ready for testing proceed to doing first");
    }

    @Override
    public void testing() {
        System.out.println("Can't proceed to testing proceed to doing first");
    }

    @Override
    public void tested() {
        System.out.println("Can't proceed to tested proceed to doing first");
    }

    @Override
    public void done() {
        System.out.println("Can't proceed to done proceed to doing first");
    }
}
