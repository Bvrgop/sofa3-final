package domain.backlog;

import java.util.ArrayList;
import java.util.List;

public class DoingState implements ItemState {
    private ItemObservable itemObservable;

    public DoingState(ItemObservable itemObservable) {
        this.itemObservable = itemObservable;
    }

    @Override
    public void toDo() {
        System.out.println("cant go back to to do");
    }

    @Override
    public void doing() {
        System.out.println("This item is already in doing");
    }

    @Override
    public void readyForTesting() {
        List<Activity> activities = this.itemObservable.getActivities();
        for(Activity a : activities){
            if(!a.isDone()){
                System.out.println("This item cant be set to ready for testing because activity: " + a.getName() + " is not done.");
                return;
            }
        }
        this.itemObservable.setState(itemObservable.getReadyForTestingState());
        this.itemObservable.notifyObservers("is ready for testing.");
    }

    @Override
    public void testing() {
        System.out.println("Can't proceed to testing proceed to ready for testing first");
    }

    @Override
    public void tested() {
        System.out.println("Can't proceed to tested proceed to ready for testing first");
    }

    @Override
    public void done() {
        System.out.println("Can't proceed to  proceed to ready for testing first");
    }
}
