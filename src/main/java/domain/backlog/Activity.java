package domain.backlog;

import domain.CustomObserver;

public class Activity {
    private String name;
    private String description;
    private boolean done;
    private CustomObserver dev;

    public Activity(String name, String description, CustomObserver dev) {
        this.name = name;
        this.description = description;
        this.dev = dev;
        this.done = false;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isDone() {
        return done;
    }

    public void setDone(boolean done) {
        this.done = done;
    }

    public CustomObserver getDev() {
        return dev;
    }

    public void setDev(CustomObserver dev) {
        this.dev = dev;
    }
}
