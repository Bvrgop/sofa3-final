package domain.backlog;

public class TestingState implements ItemState {
    private ItemObservable itemObservable;
    public TestingState(ItemObservable itemObservable) {
        this.itemObservable = itemObservable;
    }
    @Override
    public void toDo() {
        System.out.println("cant go back to to do");
    }

    @Override
    public void doing() {
        System.out.println("cant go back to to doing");
    }

    @Override
    public void readyForTesting() {
        System.out.println("cant go back to to ready for testing");
    }

    @Override
    public void testing() {
        System.out.println("This item is already in testing");
    }

    @Override
    public void tested() {
        this.itemObservable.setState(itemObservable.getTestedState());
    }

    @Override
    public void done() {
        System.out.println("Can't proceed to done proceed to tested first");

    }
}
