package domain.backlog;

public class DoneState implements ItemState {
    public DoneState(ItemObservable itemObservable) {
        this.itemObservable = itemObservable;
    }
    private ItemObservable itemObservable;

    @Override
    public void toDo() {
        System.out.println("cant go back to to do");
    }

    @Override
    public void doing() {
        System.out.println("cant go back to doing");
    }

    @Override
    public void readyForTesting() {
        System.out.println("cant go back to ready for testing");
    }

    @Override
    public void testing() {
        System.out.println("cant go back to testing");
    }

    @Override
    public void tested() {
        System.out.println("cant go back to tested");
    }

    @Override
    public void done() {
        System.out.println("This item is already in done");
    }
}
