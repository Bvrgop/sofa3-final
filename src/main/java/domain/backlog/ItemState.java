package domain.backlog;

public interface ItemState {
    void toDo();
    void doing();
    void readyForTesting();
    void testing();
    void tested();
    void done();
}
