package domain.backlog;

public class ReadyForTestingState implements ItemState {
    private ItemObservable itemObservable;
    public ReadyForTestingState(ItemObservable itemObservable) {
        this.itemObservable = itemObservable;
    }
    @Override
    public void toDo() {
        this.itemObservable.setState(itemObservable.getTodoState());
        this.itemObservable.notifyObservers("Is set back to to do."); // naar scrum master
    }

    @Override
    public void doing() {
        System.out.println("cant go back to doing");
    }

    @Override
    public void readyForTesting() {
        System.out.println("This item is already ready for testing.");
    }

    @Override
    public void testing() {
        this.itemObservable.setState(itemObservable.getTestingState());
    }

    @Override
    public void tested() {
        System.out.println("Can't proceed to tested  proceed to testing first");
    }

    @Override
    public void done() {
        System.out.println("Can't proceed to done proceed to testing first");
    }
}
