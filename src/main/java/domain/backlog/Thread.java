package domain.backlog;

import domain.CustomObserver;
import domain.sprint.ClosedState;

import java.util.ArrayList;
import java.util.List;

public class Thread {
    private ArrayList<String> messages;
    public ArrayList<CustomObserver> customObservers;
    private ItemObservable itemObservable;

    public Thread(ItemObservable o) {
        this.messages = new ArrayList<>();
        this.itemObservable = o;
        this.customObservers = new ArrayList<>();
    }

    public void sendMessage(CustomObserver o, String message) {
        if (!customObservers.contains(o)) {
            System.out.println("User is not in discussion.");
            return;
        }

        if (itemObservable.getCurrentState() instanceof DoneState) {
            System.out.println("Cannot discuss on closed item.");
        } else {
            String fullString = o.getName() + ": " + message;
            this.messages.add(fullString);
            String finalMessage = "DISCUSSION " + fullString;
            itemObservable.notifyObservers(finalMessage);
        }
    }

    protected List<CustomObserver> getCustomObservers() {
        return customObservers;
    }

    public List<String> getMessages() {
        return messages;
    }

    public void addObserver(CustomObserver o) {
        this.customObservers.add(o);
    }

    public void removeObserver(CustomObserver o) {
        this.customObservers.remove(o);
    }
}
