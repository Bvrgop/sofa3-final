package domain.backlog;

import domain.Board;
import domain.CustomObservable;
import domain.CustomObserver;
import domain.identity.Tester;
import domain.sprint.SprintObservable;

import java.util.ArrayList;
import java.util.List;

public class ItemObservable implements CustomObservable {

    private Thread discussionThread;
    private String name;
    private String description;
    private ArrayList<Activity> activities;
    private ItemState itemState;
    private ItemState todoState;
    private ItemState doingState;
    private ItemState readyForTestingState;
    private ItemState testingState;
    private ItemState testedState;
    private ItemState doneState;
    private Board board;

    private SprintObservable sprint;

    public ItemObservable(Board board, String name, String description) {
        this.name = name;
        this.description = description;
        this.todoState = new ToDoState(this);
        this.doingState = new DoingState(this);
        this.readyForTestingState = new ReadyForTestingState(this);
        this.testingState = new TestingState(this);
        this.testedState = new TestedState(this);
        this.doneState = new DoneState(this);
        this.itemState = todoState;
        this.activities = new ArrayList<>();
        this.discussionThread = new Thread(this);
        this.todoState = new ToDoState(this);
        this.board = board;
        this.sprint = null;
    }

    public void toDo(){itemState.toDo();}
    public void doing() {itemState.doing();}
    public void readyForTesting() {itemState.readyForTesting();}
    public void testing() {itemState.testing();}
    public void tested() {itemState.tested();}
    public void done() {itemState.done();}
    @Override
    public void notifyObservers(String message) {
        if(this.itemState instanceof ReadyForTestingState){
            broadcastReadyForTesting(message);
        }
        else if(this.itemState instanceof ToDoState && !message.startsWith("DISCUSSION")){
            broadcastToDo(message);
        }
        else{
            broadcastDiscussion(message);
        }

    }

    public void broadcastDiscussion(String message) {
        message = message.substring(11);
        for (CustomObserver o : discussionThread.getCustomObservers()) {
            o.update(this, message);
        }
    }

    public void broadcastReadyForTesting(String message) {
        for (CustomObserver o : board.getUsers().values()) {
            if (o.equals(sprint.getScrumMaster()) && message.startsWith("RETURNED")) {

                o.update(this, message.substring(9, message.length()));
            }
           else if(o instanceof Tester){
                o.update(this, message);
            }
        }
    }

    public void broadcastToDo(String message) {
        // voor consistentie meschien hier ook RETURNDED logica toevoegen
        for (CustomObserver o : board.getUsers().values()) {
            if (o.equals(sprint.getScrumMaster())) {
                o.update(this, message);
            }
        }
    }

    @Override
    public void addObserver(CustomObserver o) {
        this.discussionThread.addObserver(o);
    }

    @Override
    public void removeObserver(CustomObserver o) {
        this.discussionThread.removeObserver(o);
    }

    public void setState(ItemState s) {
        this.itemState = s;
    }

    public void setDiscussionThread(Thread discussionThread) {
        this.discussionThread = discussionThread;
    }

    public ItemState getCurrentState() {
        return this.itemState;
    }

    public String getName() {
        return name;
    }

    public List<Activity> getActivities() {
        return activities;
    }

    public ItemState getItemState() {
        return itemState;
    }

    public ItemState getTodoState() {
        return todoState;
    }

    public ItemState getDoingState() {
        return doingState;
    }

    public ItemState getReadyForTestingState() {
        return readyForTestingState;
    }

    public ItemState getTestingState() {
        return testingState;
    }

    public ItemState getTestedState() {
        return testedState;
    }

    public ItemState getDoneState() {
        return doneState;
    }

    public Board getBoard() {
        return board;
    }

    public void setSprint(SprintObservable sprint) {
        this.sprint = sprint;
    }

    public Thread getDiscussionThread() {
        return discussionThread;
    }

}
