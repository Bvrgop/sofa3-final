package domain.backlog;

public class TestedState implements ItemState {
    private ItemObservable itemObservable;
    public TestedState(ItemObservable itemObservable) {
        this.itemObservable = itemObservable;
    }
    @Override
    public void toDo() {
        System.out.println("cant go back to to do");
    }

    @Override
    public void doing() {
        System.out.println("cant go back to doing");
    }

    @Override
    public void readyForTesting() {
       this.itemObservable.setState(itemObservable.getReadyForTestingState());
        this.itemObservable.notifyObservers("RETURNED Is set back to ready for testing."); // naar scrum master
        this.itemObservable.notifyObservers("Item: " + this.itemObservable.getName() + ". is ready for testing"); // naar testers
    }

    @Override
    public void testing() {
        System.out.println("cant go back to testing");
    }

    @Override
    public void tested() {
        System.out.println("This item is already in tested");
    }

    @Override
    public void done() { this.itemObservable.setState(itemObservable.getDoneState());

    }
}
