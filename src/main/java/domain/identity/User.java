package domain.identity;


import domain.CustomObservable;
import domain.CustomObserver;
import domain.backlog.ItemObservable;
import domain.sprint.SprintObservable;

public abstract class User implements CustomObserver {
    private String name;

    protected User(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @Override
    public void update(CustomObservable customObservable, String message) {
        if (customObservable instanceof SprintObservable) {
            SprintObservable s = (SprintObservable) customObservable;
            System.out.println(this.name + ": Received message from " + s.getName() + ", " + message );
        } else if (customObservable instanceof ItemObservable) {
            ItemObservable i = (ItemObservable) customObservable;
            System.out.println(this.name + ": Received message from " + i.getName() + ", " + message );
        }
    }


}
