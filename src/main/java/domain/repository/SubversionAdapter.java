package domain.repository;

import java.io.File;

public class SubversionAdapter implements SCMAdapter {
    @Override
    public void commit(String s) {
        System.out.println("Subversion Commit with message: " + s);
    }

    @Override
    public void push() {
        System.out.println("Subversion push...");
    }

    @Override
    public boolean pull() {
        System.out.println("Subversion pull...");
        return true;
    }

    @Override
    public void merge(File f) {
        System.out.println("Subversion Merge...");
    }
}
