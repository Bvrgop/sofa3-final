package domain.repository;

import domain.Board;

import java.io.File;

public class Repository {
    private SCMAdapter adapter;
    private DevOpsPipeline pipeline;
    private Board board;

    public Repository(SCMAdapter adapter, Board board) {
        this.pipeline = new DevOpsPipeline(this);
        this.adapter = adapter;
        this.board = board;
    }

    public void commit(String s) {
        adapter.commit(s);
    }

    public void push() {
        adapter.push();
    }

    public boolean pull() {
        return adapter.pull();
    }

    public void merge(File f) {
        adapter.merge(f);
    }

    public boolean release() { return pipeline.run(); }

    public SCMAdapter getAdapter() {
        return adapter;
    }

    public Board getBoard() {
        return board;
    }

    public void setPipeline(DevOpsPipeline pipeline) {
        this.pipeline = pipeline;
    }
}
