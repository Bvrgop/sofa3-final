package domain.repository;

import java.io.File;

public interface SCMAdapter {
    void commit(String s);
    void push();
    boolean pull();
    void merge(File f);
}
