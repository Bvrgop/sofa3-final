package domain.repository;

import java.io.File;

public class GitAdapter implements SCMAdapter {
    @Override
    public void commit(String s) {
        System.out.println("Git Commit with message: " + s);
    }

    @Override
    public void push() {
        System.out.println("Git Push...");
    }

    @Override
    public boolean pull() {
        System.out.println("Git Pull...");
        return true;
    }

    @Override
    public void merge(File f) {
        System.out.println("Git Merge...");
    }
}
