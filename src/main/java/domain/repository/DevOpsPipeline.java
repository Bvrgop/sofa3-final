package domain.repository;

public class DevOpsPipeline extends DevOpsTemplate {
    public DevOpsPipeline(Repository repository) {
        super(repository);
    }

    @Override
    protected boolean utility() {
        System.out.println("Starting to archive previous build from " + this.repository.getBoard().getName() + "...");
        return true;
    }
}
