package domain.repository;

public enum SCMType {
    GIT,
    SUBVERSION
}
