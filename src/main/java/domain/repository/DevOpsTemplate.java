package domain.repository;

public abstract class DevOpsTemplate {
    protected Repository repository;

    protected DevOpsTemplate(Repository repository) {
        this.repository = repository;
    }

    public final boolean run() {
        System.out.println("Initiating Deployment Pipeline for " + this.repository.getBoard().getName() + "...");
        if (sources()) {
            return deployment();
        } else {
            return false;
        }
    }

    private final boolean sources() {
        System.out.println(this.repository.getBoard().getName() + ": Started to download sources from repository...");
        if (!repository.getAdapter().pull()) {
            System.out.println(this.repository.getBoard().getName() + ": Failed to download sources from repository...");
            return false;
        }
        return packages();
    }

    private final boolean packages() {
        System.out.println(this.repository.getBoard().getName() + ": Creating release package for deployment...");
        return build();
    }

    private final boolean build() {
        System.out.println(this.repository.getBoard().getName() + ": uilding Deployment Packages...");
        return test();
    }

    private final boolean test() {
        System.out.println(this.repository.getBoard().getName() + ": Running tests on Deployment packages...");
        return analyze();
    }

    private final boolean analyze() {
        System.out.println(this.repository.getBoard().getName() + ": Analyzing test results...");
        return utility();
    }

    protected abstract boolean utility();

    private final boolean deployment() {
        System.out.println("Finishing Deployment Pipeline for " + this.repository.getBoard().getName() + "...");
        System.out.println("Deploying...");
        return true;
    }

    public void setRepository(Repository repository) {
        this.repository = repository;
    }
}
