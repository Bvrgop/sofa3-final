package domain;

public interface CustomObserver {
    void update(CustomObservable customObservable, String message);
    String getName();
}
