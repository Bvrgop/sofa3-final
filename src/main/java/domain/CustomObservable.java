package domain;

public interface CustomObservable {
    void notifyObservers(String message);
    void addObserver(CustomObserver o);
    void removeObserver(CustomObserver o);
}
