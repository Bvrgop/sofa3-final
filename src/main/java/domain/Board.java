package domain;

import domain.backlog.ItemObservable;
import domain.identity.ProductOwner;
import domain.repository.*;
import domain.sprint.*;
import java.io.InvalidClassException;
import java.util.HashMap;
import java.util.Map;

public class Board {
    private Repository repository;
    private HashMap<String, SprintObservable> sprints;
    private HashMap<String, ItemObservable> backlog;
    private CustomObserver owner;
    private String name;
    private HashMap<String, CustomObserver> users;

    public Board(CustomObserver owner, String name, SCMType adapterType) throws InvalidClassException {
        if (adapterType == SCMType.GIT) {
            this.repository = new Repository(new GitAdapter(), this);
        } else {
            this.repository = new Repository(new SubversionAdapter(), this);
        }
        this.sprints = new HashMap<>();
        this.backlog = new HashMap<>();
        this.name = name;
        this.users = new HashMap<>();

        if (owner instanceof ProductOwner) {
            this.owner = owner;
            this.users.put(((ProductOwner) owner).getName(), owner);
        } else {
            throw new InvalidClassException("Owner was not of productowner class");
        }
    }

    public Map<String, SprintObservable> getSprints() {
        return sprints;
    }

    public Map<String, ItemObservable> getBacklog() {
        return backlog;
    }

    public void addUser(String name, CustomObserver o) {
        users.put(name, o);
    }

    public Map<String, CustomObserver> getUsers() {
        return users;
    }

    public Repository getRepository() {
        return repository;
    }

    public String getName() {
        return name;
    }

    public CustomObserver getOwner() {
        return owner;
    }
}
