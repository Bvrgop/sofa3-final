package domain.sprint;

import java.util.Date;

public class CreatedState implements SprintState {

    private SprintObservable sprint;

    public CreatedState(SprintObservable sprint) {
        this.sprint = sprint;
    }

    @Override
    public void created() {
        System.out.println("Sprint has been created.");
        this.sprint.setState(this.sprint.getRunningState());
        this.sprint.setStartDate(new Date(System.currentTimeMillis()));
    }

    @Override
    public void running() {
        System.out.println("Sprint is currently not running.");
    }

    @Override
    public void finished() {
        System.out.println("Sprint is currently not finished.");
    }

    @Override
    public void failed() {
        System.out.println("Sprint has not finished.");
    }

    @Override
    public void closed() {
        System.out.println("Sprint hasn't closed.");
    }
}
