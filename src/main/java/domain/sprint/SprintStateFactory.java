package domain.sprint;

public interface SprintStateFactory {
    SprintState createState(SprintObservable s);
}
