package domain.sprint;

public interface SprintState {
    void created();
    void running();
    void finished();
    void failed();
    void closed();
}
