package domain.sprint;

public enum ExportType {
    PNG,
    JPG,
    PDF
}
