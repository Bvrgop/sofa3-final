package domain.sprint;

public class CreatedStateFactory implements SprintStateFactory {
    @Override
    public SprintState createState(SprintObservable s) {
        return new CreatedState(s);
    }
}
