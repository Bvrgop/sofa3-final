package domain.sprint;

public interface SprintStrategy {
    void execute();
}
