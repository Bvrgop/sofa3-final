package domain.sprint;

public enum SprintType {
    REVIEW,
    RELEASE
}
