package domain.sprint;

public class ClosedStateFactory implements SprintStateFactory{
    @Override
    public SprintState createState(SprintObservable s) {
        return new ClosedState(s);
    }
}
