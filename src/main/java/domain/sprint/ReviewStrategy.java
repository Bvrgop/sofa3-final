package domain.sprint;

public class ReviewStrategy implements SprintStrategy {
    private SprintObservable s;

    public ReviewStrategy(SprintObservable s) {
        this.s = s;
    }

    @Override
    public void execute() {
        if (s.isReviewed()) {
            s.setState(s.getClosedState());
            s.closed();
        } else {
            s.notifyObservers("To close sprint, please upload a review file.");
        }
    }
}
