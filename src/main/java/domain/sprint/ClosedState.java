package domain.sprint;

public class ClosedState implements SprintState {

    private SprintObservable sprintObservable;

    public ClosedState(SprintObservable sprintObservable) {
        this.sprintObservable = sprintObservable;
    }

    @Override
    public void created() {
        System.out.println("Cannot Create: Sprint is closed.");
    }

    @Override
    public void running() {
        System.out.println("Cannot Run: Sprint is closed.");
    }

    @Override
    public void finished() {
        System.out.println("Cannot Finish: Sprint is closed.");
    }

    @Override
    public void failed() {
        System.out.println("Cannot Fail: Sprint is closed.");
    }

    @Override
    public void closed() {
        sprintObservable.notifyObservers("Sprint has been successfully finished and closed.");
    }
}
