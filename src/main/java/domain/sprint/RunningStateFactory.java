package domain.sprint;

public class RunningStateFactory implements SprintStateFactory {
    @Override
    public SprintState createState(SprintObservable s) {
        return new RunningState(s);
    }
}
