package domain.sprint;

import java.util.Date;

public class RunningState implements SprintState {

    private SprintObservable sprint;

    public RunningState(SprintObservable sprint) {
        this.sprint = sprint;
    }

    @Override
    public void created() {
        System.out.println("Sprint is already running.");
    }

    @Override
    public void running() {
        Date d = new Date(System.currentTimeMillis());
        long runTimeInMillis = (long) this.sprint.getDurationInDays() * 86400000;
        long endTime = this.sprint.getStartDate().getTime() + runTimeInMillis;
        if (d.getTime() >= endTime) {
            this.sprint.setState(this.sprint.getFinishedState());
        } else {
            System.out.println("Sprint is running.");
        }
    }

    @Override
    public void finished() {
        System.out.println("Sprint is still running.");
    }

    @Override
    public void failed() {
        System.out.println("Sprint has not finished.");
    }

    @Override
    public void closed() {
        System.out.println("Sprint hasn't closed.");
    }
}
