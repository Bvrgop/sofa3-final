package domain.sprint;

import domain.Board;
import domain.CustomObservable;
import domain.CustomObserver;
import domain.backlog.ItemObservable;
import domain.identity.ScrumMaster;
import java.io.InvalidClassException;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class SprintObservable implements CustomObservable {
    private SprintStrategy sprintStrategy;
    private HashMap<String, ItemObservable> items;
    private SprintState createdState;
    private SprintState runningState;
    private SprintState finishedState;
    private SprintState failedState;
    private SprintState closedState;
    private SprintState currentState;
    private CustomObserver scrumMaster;
    private Board board;
    private String name;
    private int durationInDays;
    private Date startDate;
    private boolean acceptableQuality;
    private boolean isReviewed;

    public SprintObservable(Board b,
                            CustomObserver scrumMaster,
                            SprintType type,
                            SprintStateFactory createdStateFactory,
                            SprintStateFactory runningStateFactory,
                            SprintStateFactory finishedStateFactory,
                            SprintStateFactory failedStateFactory,
                            SprintStateFactory closedStateFactory,
                            String name,
                            int durationInDays) throws InvalidClassException {
        this.items = new HashMap<>();
        this.createdState = createdStateFactory.createState(this);
        this.runningState = runningStateFactory.createState(this);
        this.finishedState = finishedStateFactory.createState(this);
        this.failedState = failedStateFactory.createState(this);
        this.closedState = closedStateFactory.createState(this);
        this.currentState = createdState;
        this.board = b;
        this.name = name;
        this.durationInDays = durationInDays;
        this.acceptableQuality = false;
        this.isReviewed = false;

        if (type == SprintType.RELEASE) {
            this.sprintStrategy = new ReleaseStrategy(this);
        } else {
            this.sprintStrategy = new ReviewStrategy(this);
        }

        if (scrumMaster instanceof ScrumMaster) {
            this.scrumMaster = scrumMaster;
        } else {
            throw new InvalidClassException("User was not a scrummaster");
        }
    }

    @Override
    public void notifyObservers(String message) {
        if (this.currentState instanceof FailedState || this.currentState instanceof ClosedState) {
            board.getOwner().update(this, message);
            this.scrumMaster.update(this, message);
        } else if (this.currentState instanceof FinishedState) {
            this.scrumMaster.update(this, message);
        } else {
            Iterator<CustomObserver> iterator = board.getUsers().values().iterator();
            while (iterator.hasNext()) {
                iterator.next().update(this, message);
            }
        }
    }

    @Override
    public void addObserver(CustomObserver o) {
        //Observers are pulled from board.
        throw new UnsupportedOperationException();
    }

    @Override
    public void removeObserver(CustomObserver o) {
        //Observers are pulled and removed from board.
        throw new UnsupportedOperationException();
    }

    public void setState(SprintState state) {
        if (this.currentState instanceof FinishedState) {
            if (state instanceof ClosedState || state instanceof FailedState) {
                this.currentState = state;
            } else {
                System.out.println(this.name + ": Cannot edit sprint while release is ongoing.");
            }
            return;
        }

        this.currentState = state;
    }

    public String getName() {
        return name;
    }

    public void export(ExportType t, boolean addHeaderAndFooter) {
        ExportStrategy strategy = new PdfStrategy(this);
        if (t == ExportType.JPG) {
            strategy = new JpgStrategy(this);
        } else if (t == ExportType.PNG) {
            strategy = new PngStrategy(this);
        }
        strategy.execute(addHeaderAndFooter);
    }

    public void uploadReview(CustomObserver scrumMaster, boolean file) {
        if (!scrumMaster.equals(this.scrumMaster)) {
            return;
        }
        if (file) {
            this.isReviewed = true;
            notifyObservers("Successfully uploaded review.");
        }
    }

    public void closeSprint() {
        sprintStrategy.execute();
    }

    public void cancelRelease() {
        setState(getClosedState());
        closed();
    }

    public Map<String, ItemObservable> getItems() {
        return items;
    }

    public SprintState getRunningState() {
        return runningState;
    }

    public SprintState getFinishedState() {
        return finishedState;
    }

    public SprintState getFailedState() {
        return failedState;
    }

    public SprintState getClosedState() {
        return closedState;
    }

    public SprintStrategy getSprintStrategy() {
        return sprintStrategy;
    }

    public int getDurationInDays() {
        return durationInDays;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public boolean isAcceptableQuality() {
        return acceptableQuality;
    }

    public void setAcceptableQuality(boolean acceptableQuality) {
        this.acceptableQuality = acceptableQuality;
    }

    public Board getBoard() {
        return board;
    }

    public void created() {
        this.currentState.created();
    }

    public void running() {
        this.currentState.running();
    }

    public void finished() {
        this.currentState.finished();
    }

    public void failed() { this.currentState.failed(); }

    public void closed() { this.currentState.closed(); }

    public CustomObserver getScrumMaster() {
        return scrumMaster;
    }

    public boolean isReviewed() {
        return isReviewed;
    }

    public SprintState getCurrentState() {
        return currentState;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();

        builder.append("Name: " + this.name + "\n");
        if(startDate != null) {
            builder.append("Start Date: " + this.startDate + "\n");
        } else {
            builder.append("Start Date: Unknown" + "\n");
        }
        builder.append("Duration in Days: " + this.durationInDays + "\n");
        builder.append("Current Sprint State: " + this.currentState.getClass().getSimpleName() + "\n");
        builder.append("Is acceptable quality: " + this.acceptableQuality + "\n");
        builder.append("Is reviewed: " + this.isReviewed + "\n");
        builder.append("Sprint Members:\n");

        Iterator<CustomObserver> i = this.board.getUsers().values().iterator();
        while(i.hasNext()) {
            CustomObserver observer = i.next();
            builder.append("- " + observer.getName() + ": " + observer.getClass().getSimpleName() + "\n");
        }

        return builder.toString();
    }

    public void setName(String name) {
        if (currentState instanceof CreatedState) {
            this.name = name;
        }
    }

    public void setDurationInDays(int durationInDays) {
        if (currentState instanceof CreatedState) {
            this.durationInDays = durationInDays;
        }
    }
}
