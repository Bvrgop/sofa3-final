package domain.sprint;

import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;

public class JpgStrategy implements ExportStrategy {

    private SprintObservable sprint;

    public JpgStrategy(SprintObservable sprint) {
        this.sprint = sprint;
    }

    @Override
    public void execute(boolean addHeaderAndFooter) {
        File file = new File(this.sprint.getName() + "_jpg_output.txt");
        try(FileWriter fileWriter = new FileWriter(file); PrintWriter printWriter = new PrintWriter(fileWriter);) {
            if (addHeaderAndFooter) {
                StringBuilder builder = new StringBuilder();

                builder.append("Avans DevOps Sprint Report:\n");
                builder.append("- - - - - - - - - - - - - - - -\n\n");
                builder.append(this.sprint.toString());
                builder.append("\n\n");
                builder.append("- - - - - - - - - - - - - - - -\n");
                builder.append("Footer");

                printWriter.print(builder);
            } else {
                printWriter.print(this.sprint.toString());
            }
        } catch (Exception e) {
            System.out.println("Oh shit");
        }
    }
}
