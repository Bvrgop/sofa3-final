package domain.sprint;

public class ReleaseStrategy implements SprintStrategy {
    private SprintObservable s;

    public ReleaseStrategy(SprintObservable s) {
        this.s = s;
    }

    @Override
    public void execute() {
        if (s.isAcceptableQuality()) {
            boolean success = s.getBoard().getRepository().release();
            if (success) {
                s.setState(s.getClosedState());
                s.closed();
            } else {
                s.notifyObservers("Release has failed.");
                s.setState(s.getFinishedState());
            }
        } else {
            s.setState(s.getFailedState());
            s.failed();
        }
    }
}
