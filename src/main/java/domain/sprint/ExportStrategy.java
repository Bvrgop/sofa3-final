package domain.sprint;

public interface ExportStrategy {
    void execute(boolean addHeaderAndFooter);
}
