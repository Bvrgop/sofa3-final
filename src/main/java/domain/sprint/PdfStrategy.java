package domain.sprint;

import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;

public class PdfStrategy implements ExportStrategy {
    private SprintObservable sprint;

    public PdfStrategy(SprintObservable sprint) {
        this.sprint = sprint;
    }

    @Override
    public void execute(boolean addHeaderAndFooter) {
        File file = new File(this.sprint.getName() + "_pdf_output.txt");
        try(FileWriter fileWriter = new FileWriter(file); PrintWriter printWriter = new PrintWriter(fileWriter);) {
            if (addHeaderAndFooter) {
                StringBuilder builder = new StringBuilder();

                builder.append("Avans DevOps Sprint Report:\n");
                builder.append("- - - - - - - - - - - - - - - -\n\n");
                builder.append(this.sprint.toString());
                builder.append("\n\n");
                builder.append("- - - - - - - - - - - - - - - -\n");
                builder.append("Footer");

                printWriter.print(builder);
            } else {
                printWriter.print(this.sprint.toString());
            }
        } catch (Exception e) {
            System.out.println("Oh shit");
        }
    }
}
