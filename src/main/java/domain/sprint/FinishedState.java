package domain.sprint;

public class FinishedState implements SprintState {

    private SprintObservable sprint;

    public FinishedState(SprintObservable sprint) {
        this.sprint = sprint;
    }

    @Override
    public void created() {
        System.out.println("Cannot Create: Sprint has already finished.");
    }

    @Override
    public void running() {
        System.out.println("Cannot Run: Sprint has already finished.");
    }

    @Override
    public void finished() {
        this.sprint.notifyObservers("Sprint has finished.");
        this.sprint.getSprintStrategy().execute();
    }

    @Override
    public void failed() {
        System.out.println("Sprint has not failed.");
    }

    @Override
    public void closed() {
        System.out.println("Sprint hasn't closed.");
    }
}
