package domain.sprint;

public class FailedState implements SprintState {

    private SprintObservable sprint;

    public FailedState(SprintObservable sprint) {
        this.sprint = sprint;
    }

    @Override
    public void created() {
        System.out.println("Sprint has not failed.");
    }

    @Override
    public void running() {
        System.out.println("Sprint is not running.");
    }

    @Override
    public void finished() {
        System.out.println("Sprint has finished but failed.");
    }

    @Override
    public void failed() {
        sprint.notifyObservers("Sprint has failed to release.");
    }

    @Override
    public void closed() {
        System.out.println("Sprint hasn't closed.");
    }
}
