package domain.sprint;

public class FailedStateFactory implements SprintStateFactory {
    @Override
    public SprintState createState(SprintObservable s) {
        return new FailedState(s);
    }
}
