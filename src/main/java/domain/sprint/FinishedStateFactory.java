package domain.sprint;

public class FinishedStateFactory implements SprintStateFactory {
    @Override
    public SprintState createState(SprintObservable s) {
        return new FinishedState(s);
    }
}
