import domain.Board;
import domain.backlog.Activity;
import domain.backlog.ItemObservable;
import domain.identity.Developer;
import domain.identity.ProductOwner;
import domain.identity.ScrumMaster;
import domain.repository.DevOpsPipeline;
import domain.repository.Repository;
import domain.repository.SCMType;
import domain.sprint.*;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.*;
import java.util.Scanner;


import static org.junit.jupiter.api.Assertions.*;

public class SprintTests {
    private final PrintStream standardOut = System.out;
    private final ByteArrayOutputStream outputStreamCaptor = new ByteArrayOutputStream();

    private Board board;
    private SprintObservable sprint;
    private ItemObservable item;
    private Activity activity;
    private ProductOwner owner;
    private ScrumMaster scrumMaster;
    private Developer developer;
    private SprintStateFactory createdFactory;
    private SprintStateFactory runningFactory;
    private SprintStateFactory finishedFactory;
    private SprintStateFactory failedFactory;
    private SprintStateFactory closedFactory;

    @BeforeEach
    public void init() {
        try {
            owner = new ProductOwner("Test owner");
            scrumMaster = new ScrumMaster("Test master");
            developer = new Developer("Test developer");
            board = new Board(owner, "Test board", SCMType.GIT);
            item = new ItemObservable(board, "Test item", "description");
            activity = new Activity("Test Activity", "description", developer);
            item.getActivities().add(activity);
            createdFactory = new CreatedStateFactory();
            runningFactory = new RunningStateFactory();
            finishedFactory = new FinishedStateFactory();
            failedFactory = new FailedStateFactory();
            closedFactory = new ClosedStateFactory();
        } catch (InvalidClassException e) {

        }
    }

    @BeforeEach
    public void setUp() {
        System.setOut(new PrintStream(outputStreamCaptor));
    }

    @AfterEach
    public void tearDown() {
        board = null;
        sprint = null;
        scrumMaster = null;
        item = null;
        owner = null;
        activity = null;
        developer = null;
        System.setOut(standardOut);
    }

    @Test
    public void reviewStrategyShouldDenyCompletionWithoutReview() {
        try {
            sprint = new SprintObservable(board, scrumMaster, SprintType.REVIEW, createdFactory, runningFactory, finishedFactory, failedFactory, closedFactory, "Test Sprint", 1);
            sprint.setState(sprint.getFinishedState());
            sprint.closeSprint();

            assertTrue(sprint.getCurrentState() instanceof FinishedState);
            assertEquals(this.scrumMaster.getName() + ": Received message from " + sprint.getName() + ", To close sprint, please upload a review file.".trim(), outputStreamCaptor.toString().trim());
        } catch (InvalidClassException e) {

        }
    }

    @Test
    public void reviewStrategyShouldComplete() {
        try {
            sprint = new SprintObservable(board, scrumMaster, SprintType.REVIEW, createdFactory, runningFactory, finishedFactory, failedFactory, closedFactory, "Test Sprint", 1);
            sprint.setState(sprint.getFinishedState());
            sprint.uploadReview(scrumMaster, true);
            sprint.closeSprint();

            String[] listOfMessages = outputStreamCaptor.toString().trim().split("\\.");

            assertTrue(sprint.getCurrentState() instanceof ClosedState);
            assertEquals(this.scrumMaster.getName() + ": Received message from " + sprint.getName() + ", Successfully uploaded review".trim(), listOfMessages[0].trim());
            assertEquals(this.owner.getName() + ": Received message from " + sprint.getName() + ", Sprint has been successfully finished and closed".trim(), listOfMessages[1].trim());
            assertEquals(this.scrumMaster.getName() + ": Received message from " + sprint.getName() + ", Sprint has been successfully finished and closed".trim(), listOfMessages[2].trim());
        } catch (InvalidClassException e) {

        }
    }

    @Test
    public void releaseStrategyShouldFailIfNotAcceptable() {
        try {
            sprint = new SprintObservable(board, scrumMaster, SprintType.RELEASE, createdFactory, runningFactory, finishedFactory, failedFactory, closedFactory, "Test Sprint", 1);
            sprint.setState(sprint.getFinishedState());
            sprint.setAcceptableQuality(false);
            sprint.finished();

            String[] listOfMessages = outputStreamCaptor.toString().trim().split("\\.");

            assertTrue(sprint.getCurrentState() instanceof FailedState);
            assertEquals(this.scrumMaster.getName() + ": Received message from " + sprint.getName() + ", Sprint has finished".trim(), listOfMessages[0].trim());
            assertEquals(this.owner.getName() + ": Received message from " + sprint.getName() + ", Sprint has failed to release".trim(), listOfMessages[1].trim());
            assertEquals(this.scrumMaster.getName() + ": Received message from " + sprint.getName() + ", Sprint has failed to release".trim(), listOfMessages[2].trim());
        } catch (InvalidClassException e) {

        }
    }

    @Test
    public void releaseStrategyShouldSucceed() {
        try {
            sprint = new SprintObservable(board, scrumMaster, SprintType.RELEASE, createdFactory, runningFactory, finishedFactory, failedFactory, closedFactory, "Test Sprint", 1);
            sprint.setState(sprint.getFinishedState());
            sprint.setAcceptableQuality(true);
            sprint.finished();

            String[] listOfMessages = outputStreamCaptor.toString().trim().split("\\.");

            assertTrue(sprint.getCurrentState() instanceof ClosedState);
            assertEquals(this.owner.getName() + ": Received message from " + sprint.getName() + ", Sprint has been successfully finished and closed", listOfMessages[31].trim());
            assertEquals(this.scrumMaster.getName() + ": Received message from " + sprint.getName() + ", Sprint has been successfully finished and closed", listOfMessages[32].trim());
        } catch (InvalidClassException e) {

        }
    }

    @Test
    public void sprintShouldGoIntoRunningState() {
        try {
            sprint = new SprintObservable(board, scrumMaster, SprintType.RELEASE, createdFactory, runningFactory, finishedFactory, failedFactory, closedFactory, "Test Sprint", 1);
            sprint.created();

            assertTrue(sprint.getCurrentState() instanceof RunningState);
        } catch (Exception e) {

        }
    }

    @Test
    public void sprintShouldGoIntoFinishedState() {
        try {
            sprint = new SprintObservable(board, scrumMaster, SprintType.RELEASE, createdFactory, runningFactory, finishedFactory, failedFactory, closedFactory, "Test Sprint", 0);
            sprint.created();
            sprint.running();
            assertTrue(sprint.getCurrentState() instanceof FinishedState);
        } catch (Exception e) {

        }
    }

    @Test
    public void sprintShouldNotGoIntoFinishedState() {
        try {
            sprint = new SprintObservable(board, scrumMaster, SprintType.RELEASE, createdFactory, runningFactory, finishedFactory, failedFactory, closedFactory, "Test Sprint", 1);
            sprint.created();
            sprint.running();

            String[] listOfMessages = outputStreamCaptor.toString().trim().split("\n");

            assertTrue(sprint.getCurrentState() instanceof RunningState);
            assertEquals("Sprint is running.", listOfMessages[1]);
        } catch (Exception e) {

        }
    }

    @Test
    public void sprintShouldGoIntoFailedState() {
        try {
            sprint = new SprintObservable(board, scrumMaster, SprintType.RELEASE, createdFactory, runningFactory, finishedFactory, failedFactory, closedFactory, "Test Sprint", 0);
            sprint.setAcceptableQuality(false);
            sprint.created();
            sprint.running();
            sprint.finished();

            assertTrue(sprint.getCurrentState() instanceof FailedState);
        } catch (Exception e) {

        }
    }

    @Test
    public void sprintShouldBroadcastFail() {
        try {
            sprint = new SprintObservable(board, scrumMaster, SprintType.RELEASE, createdFactory, runningFactory, finishedFactory, failedFactory, closedFactory, "Test Sprint", 0);
            sprint.setAcceptableQuality(false);
            sprint.created();
            sprint.running();
            sprint.finished();

            String[] listOfMessages = outputStreamCaptor.toString().trim().split("\n");

            assertTrue(sprint.getCurrentState() instanceof FailedState);
            assertEquals(this.owner.getName() + ": Received message from " + this.sprint.getName() + ", Sprint has failed to release.", listOfMessages[2].trim());
            assertEquals(this.scrumMaster.getName() + ": Received message from " + this.sprint.getName() + ", Sprint has failed to release.", listOfMessages[3].trim());
        } catch (Exception e) {

        }
    }

    @Test
    public void sprintShouldGoIntoClosedState() {
        try {
            sprint = new SprintObservable(board, scrumMaster, SprintType.RELEASE, createdFactory, runningFactory, finishedFactory, failedFactory, closedFactory, "Test Sprint", 0);
            sprint.setAcceptableQuality(true);
            sprint.created();
            sprint.running();
            sprint.finished();

            assertTrue(sprint.getCurrentState() instanceof ClosedState);
        } catch (Exception e) {

        }
    }

    @Test
    public void sprintShouldBroadcastClosing() {
        try {
            sprint = new SprintObservable(board, scrumMaster, SprintType.RELEASE, createdFactory, runningFactory, finishedFactory, failedFactory, closedFactory, "Test Sprint", 0);
            sprint.setAcceptableQuality(true);
            sprint.created();
            sprint.running();
            sprint.finished();

            String[] listOfMessages = outputStreamCaptor.toString().trim().split("\n");

            assertTrue(sprint.getCurrentState() instanceof ClosedState);
            assertEquals(this.owner.getName() + ": Received message from " + this.sprint.getName() + ", Sprint has been successfully finished and closed.", listOfMessages[12].trim());
            assertEquals(this.scrumMaster.getName() + ": Received message from " + this.sprint.getName() + ", Sprint has been successfully finished and closed.", listOfMessages[13].trim());
        } catch (Exception e) {

        }
    }

    @Test
    public void sprintShouldExportInPDF() {
        try {
            sprint = new SprintObservable(board, scrumMaster, SprintType.RELEASE, createdFactory, runningFactory, finishedFactory, failedFactory, closedFactory, "Test Sprint", 0);
            sprint.setAcceptableQuality(true);
            sprint.created();
            sprint.running();
            sprint.finished();

            sprint.export(ExportType.PDF, true);
            File newFile = new File(sprint.getName() + "_pdf_output.txt");
            assertTrue(newFile.exists());
            newFile.delete();
        } catch (Exception e) {

        }
    }

    @Test
    public void sprintShouldExportInPNG() {
        try {
            sprint = new SprintObservable(board, scrumMaster, SprintType.RELEASE, createdFactory, runningFactory, finishedFactory, failedFactory, closedFactory, "Test Sprint", 0);
            sprint.setAcceptableQuality(true);
            sprint.created();
            sprint.running();
            sprint.finished();

            sprint.export(ExportType.PNG, true);
            File newFile = new File(sprint.getName() + "_png_output.txt");
            assertTrue(newFile.exists());
            newFile.delete();
        } catch (Exception e) {

        }
    }

    @Test
    public void sprintShouldExportInJPG() {
        try {
            sprint = new SprintObservable(board, scrumMaster, SprintType.RELEASE, createdFactory, runningFactory, finishedFactory, failedFactory, closedFactory, "Test Sprint", 0);
            sprint.setAcceptableQuality(true);
            sprint.created();
            sprint.running();
            sprint.finished();

            sprint.export(ExportType.JPG, true);
            File newFile = new File(sprint.getName() + "_jpg_output.txt");
            assertTrue(newFile.exists());
            newFile.delete();
        } catch (Exception e) {

        }
    }

    @Test
    public void sprintShouldExportWithHeaderAndFooter() {
        try {
            sprint = new SprintObservable(board, scrumMaster, SprintType.RELEASE, createdFactory, runningFactory, finishedFactory, failedFactory, closedFactory, "Test Sprint", 0);
            sprint.setAcceptableQuality(true);
            sprint.created();
            sprint.running();
            sprint.finished();

            sprint.export(ExportType.JPG, true);
            sprint.export(ExportType.PNG, true);
            sprint.export(ExportType.PDF, true);
            File jpgFile = new File(sprint.getName() + "_jpg_output.txt");
            File pdfFile = new File(sprint.getName() + "_pdf_output.txt");
            File pngFile = new File(sprint.getName() + "_png_output.txt");

            Scanner jpgScanner = new Scanner(jpgFile);
            Scanner pdfScanner = new Scanner(pdfFile);
            Scanner pngScanner = new Scanner(pngFile);

            assertEquals("Avans DevOps Sprint Report:", jpgScanner.nextLine());
            assertEquals("Avans DevOps Sprint Report:", pdfScanner.nextLine());
            assertEquals("Avans DevOps Sprint Report:", pngScanner.nextLine());

            jpgScanner.close();
            pdfScanner.close();
            pngScanner.close();
            jpgFile.delete();
            pdfFile.delete();
            pngFile.delete();
        } catch (Exception e) {

        }
    }

    @Test
    public void sprintShouldExportWithoutHeaderAndFooter() {
        try {
            sprint = new SprintObservable(board, scrumMaster, SprintType.RELEASE, createdFactory, runningFactory, finishedFactory, failedFactory, closedFactory, "Test Sprint", 0);
            sprint.setAcceptableQuality(true);
            sprint.created();
            sprint.running();
            sprint.finished();

            sprint.export(ExportType.JPG, false);
            sprint.export(ExportType.PNG, false);
            sprint.export(ExportType.PDF, false);
            File jpgFile = new File(sprint.getName() + "_jpg_output.txt");
            File pdfFile = new File(sprint.getName() + "_pdf_output.txt");
            File pngFile = new File(sprint.getName() + "_png_output.txt");

            Scanner jpgScanner = new Scanner(jpgFile);
            Scanner pdfScanner = new Scanner(pdfFile);
            Scanner pngScanner = new Scanner(pngFile);

            assertEquals("Name: " + sprint.getName(), jpgScanner.nextLine());
            assertEquals("Name: " + sprint.getName(), pdfScanner.nextLine());
            assertEquals("Name: " + sprint.getName(), pngScanner.nextLine());

            jpgScanner.close();
            pdfScanner.close();
            pngScanner.close();
            jpgFile.delete();
            pdfFile.delete();
            pngFile.delete();
        } catch (Exception e) {

        }
    }
}
