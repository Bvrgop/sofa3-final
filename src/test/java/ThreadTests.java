import domain.Board;
import domain.backlog.*;
import domain.backlog.Thread;
import domain.identity.Developer;
import domain.identity.ProductOwner;
import domain.identity.ScrumMaster;
import domain.identity.Tester;
import domain.repository.SCMType;
import domain.sprint.*;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InvalidClassException;
import java.io.PrintStream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class ThreadTests {
    private final PrintStream standardOut = System.out;
    private final ByteArrayOutputStream outputStreamCaptor = new ByteArrayOutputStream();
    private Board board;
    private SprintObservable sprint;
    private ScrumMaster scrumMaster;
    private ItemObservable item;
    private Activity activity;
    private Developer dev;
    private Thread thread;


    @BeforeEach
    public  void init(){
        board = null;
        sprint = null;
        scrumMaster = new ScrumMaster("Scrum master");
        try{
            SprintStateFactory createdStateFactory = new CreatedStateFactory();
            SprintStateFactory runningStateFactory = new RunningStateFactory();
            SprintStateFactory finishedStateFactory = new FinishedStateFactory();
            SprintStateFactory failedStateFactory = new FailedStateFactory();
            SprintStateFactory closedSprintFactory = new ClosedStateFactory();

            board = new Board(new ProductOwner("Test"), "Test board", SCMType.GIT);

            sprint = new SprintObservable(board,scrumMaster ,SprintType.REVIEW, createdStateFactory, runningStateFactory, finishedStateFactory, failedStateFactory, closedSprintFactory, "Test sprint", 10);
            board.addUser(scrumMaster.getName(), scrumMaster);
            item = new ItemObservable(board,"Test item", "This is a test item");
            dev = new Developer("Test dev");
            activity = new Activity("TestActivity", "this is a test", dev);
            activity.setDone(true);
            item.getActivities().add(activity);
            item.setSprint(sprint);

            thread = new Thread(item);
            thread.addObserver(dev);
        }catch (InvalidClassException e){

        }
    }
    @BeforeEach
    public void setUp() {
        System.setOut(new PrintStream(outputStreamCaptor));
    }

    @AfterEach
    public void tearDown() throws IOException {
        board = null;
        sprint = null;
        scrumMaster = null;
        item = null;
        thread = null;
        System.setOut(standardOut);
        // Runtime.getRuntime().exec("cls");
    }

    @Test
    public void sendMessageTestPath1_2(){
        thread.sendMessage(new Developer("Test dev 2"), "This is a test");
        assertEquals("User is not in discussion.".trim(), outputStreamCaptor.toString().trim());
    }
    @Test
    public void sendMessageTestPath1_3_4(){
        item.setState(new DoneState(item));
        thread.sendMessage(dev, "This is a test");
        assertEquals("Cannot discuss on closed item.".trim(), outputStreamCaptor.toString().trim());
    }
    @Test
    public void sendMessageTestPath1_3_5(){
        item.setDiscussionThread(thread);
        thread.sendMessage(dev, "This is a test.");
        assertEquals("Test dev: Received message from Test item, Test dev: This is a test.".trim(), outputStreamCaptor.toString().trim());
    }
}