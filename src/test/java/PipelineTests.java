import domain.Board;
import domain.backlog.Activity;
import domain.backlog.ItemObservable;
import domain.identity.Developer;
import domain.identity.ProductOwner;
import domain.identity.ScrumMaster;
import domain.repository.SCMType;
import domain.sprint.*;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import java.io.ByteArrayOutputStream;
import java.io.InvalidClassException;
import java.io.PrintStream;

import static org.junit.jupiter.api.Assertions.*;

public class PipelineTests {
    private final PrintStream standardOut = System.out;
    private final ByteArrayOutputStream outputStreamCaptor = new ByteArrayOutputStream();

    private Board board;
    private SprintObservable sprint;
    private ItemObservable item;
    private Activity activity;
    private ProductOwner owner;
    private ScrumMaster scrumMaster;
    private Developer developer;
    private SprintStateFactory createdFactory;
    private SprintStateFactory runningFactory;
    private SprintStateFactory finishedFactory;
    private SprintStateFactory failedFactory;
    private SprintStateFactory closedFactory;

    @BeforeEach
    public void init() {
        try {
            createdFactory = new CreatedStateFactory();
            runningFactory = new RunningStateFactory();
            finishedFactory = new FinishedStateFactory();
            failedFactory = new FailedStateFactory();
            closedFactory = new ClosedStateFactory();
            owner = new ProductOwner("Test owner");
            scrumMaster = new ScrumMaster("Test master");
            developer = new Developer("Test developer");
            board = new Board(owner, "Test board", SCMType.GIT);
            item = new ItemObservable(board, "Test item", "description");
            activity = new Activity("Test Activity", "description", developer);
            sprint = new SprintObservable(board, scrumMaster, SprintType.RELEASE, createdFactory, runningFactory, finishedFactory, failedFactory, closedFactory, "Test Sprint", 1);
            item.getActivities().add(activity);
        } catch (InvalidClassException e) {

        }
    }

    @BeforeEach
    public void setUp() {
        System.setOut(new PrintStream(outputStreamCaptor));
    }

    @AfterEach
    public void tearDown() {
        board = null;
        sprint = null;
        scrumMaster = null;
        item = null;
        owner = null;
        activity = null;
        developer = null;
        createdFactory = null;
        runningFactory = null;
        finishedFactory = null;
        failedFactory = null;
        closedFactory = null;
        System.setOut(standardOut);
    }

    @Test
    public void pipelineShouldSucceedAndBroadcastMessage() {
        sprint.setState(sprint.getFinishedState());
        sprint.setAcceptableQuality(true);
        sprint.finished();

        String[] listOfMessages = outputStreamCaptor.toString().trim().split("\n");

        assertTrue(sprint.getCurrentState() instanceof ClosedState);
        assertEquals("Deploying...", listOfMessages[10].trim());
        assertEquals(owner.getName() + ": Received message from " + sprint.getName() + ", Sprint has been successfully finished and closed.", listOfMessages[11].trim());
        assertEquals(scrumMaster.getName() + ": Received message from " + sprint.getName() + ", Sprint has been successfully finished and closed.", listOfMessages[12].trim());
    }
}
