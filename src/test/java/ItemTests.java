import domain.Board;
import domain.backlog.*;
import domain.backlog.Thread;
import domain.identity.Developer;
import domain.identity.ProductOwner;
import domain.identity.ScrumMaster;
import domain.identity.Tester;
import domain.repository.SCMType;
import domain.sprint.*;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InvalidClassException;
import java.io.PrintStream;

import static org.junit.jupiter.api.Assertions.*;
class ItemTests {
    private final PrintStream standardOut = System.out;
    private final ByteArrayOutputStream outputStreamCaptor = new ByteArrayOutputStream();
    private Board board;
    private SprintObservable sprint;
    private ScrumMaster scrumMaster;
    private ItemObservable item;
    private Activity activity;
    private Developer dev;

    @BeforeEach
    public void init(){
        board = null;
        sprint = null;
        scrumMaster = new ScrumMaster("Scrum master");
        try{
            SprintStateFactory createdStateFactory = new CreatedStateFactory();
            SprintStateFactory runningStateFactory = new RunningStateFactory();
            SprintStateFactory finishedStateFactory = new FinishedStateFactory();
            SprintStateFactory failedStateFactory = new FailedStateFactory();
            SprintStateFactory closedSprintFactory = new ClosedStateFactory();

            board = new Board(new ProductOwner("Test"), "Test board", SCMType.GIT);

            sprint = new SprintObservable(board,scrumMaster ,SprintType.REVIEW, createdStateFactory, runningStateFactory, finishedStateFactory, failedStateFactory, closedSprintFactory, "Test sprint", 10);
            board.addUser(scrumMaster.getName(), scrumMaster);
            item = new ItemObservable(board,"Test item", "This is a test item");
            dev = new Developer("Test dev");
            activity = new Activity("TestActivity", "this is a test", dev);
            activity.setDone(true);
            item.getActivities().add(activity);
            item.setSprint(sprint);
        }catch (InvalidClassException e){

        }
    }
    @BeforeEach
    public void setUp() {
        System.setOut(new PrintStream(outputStreamCaptor));
    }

    @AfterEach
    public void tearDown() throws IOException {
        board = null;
        sprint = null;
        scrumMaster = null;
        item = null;
        System.setOut(standardOut);
       // Runtime.getRuntime().exec("cls");
    }

    @Test
    public void GraphPath1_2_4_5_7(){

        item.doing();
        item.readyForTesting();
        item.testing();
        item.tested();
        item.done();

        assertTrue(item.getItemState() instanceof DoneState);
    }
    @Test
    public void GraphPath1_2_3_1_2_4_5_7(){

        item.doing();
        item.readyForTesting();
        item.toDo();
        item.doing();
        item.readyForTesting();
        item.testing();
        item.tested();
        item.done();

        assertTrue(item.getItemState() instanceof DoneState);
    }
    @Test
    public void GraphPath1_2_4_5_6_4_5_7(){

        item.doing();
        item.readyForTesting();
        item.testing();
        item.tested();
        item.readyForTesting();
        item.testing();
        item.tested();
        item.done();

        assertTrue(item.getItemState() instanceof DoneState);
    }
    @Test
    public void GraphPath1_2_4_5_6_3_1_2_4_5_7(){

        item.doing();
        item.readyForTesting();
        item.toDo();
        item.doing();
        item.readyForTesting();
        item.testing();
        item.tested();
        item.readyForTesting();
        item.testing();
        item.tested();
        item.done();

        assertTrue(item.getItemState() instanceof DoneState);
    }
    @Test
    public void scrumMasterShouldReceiveNotificationWhenItemIsSetBackToReadyForTesting(){



        item.doing();
        item.readyForTesting();
        item.testing();
        item.tested();
        item.readyForTesting();

        assertTrue(item.getItemState() instanceof ReadyForTestingState);
        assertEquals("Scrum master: Received message from Test item, Is set back to ready for testing.".trim(), outputStreamCaptor.toString().trim());
    }
    @Test
    public void notifyObserversTestPath1_2(){
        board.addUser("Tester1", new Tester("Tester1"));
        ReadyForTestingState readyForTestingState = new ReadyForTestingState(item);

        item.setState(readyForTestingState);
        item.notifyObservers("Dit is een test bericht.");


     //   assertTrue(item.getItemState() instanceof ReadyForTestingState);
        assertEquals("Tester1: Received message from Test item, Dit is een test bericht.".trim(), outputStreamCaptor.toString().trim());
    }
    @Test
    public void notifyObserversTestPath1_3_4(){
        //board.addUser("Tester1",new ScrumMaster("Tester1"));
        // board.addUser("Tester2",new Tester("Tester2"));
        ToDoState toDoState = new ToDoState(item);

        item.setState(toDoState);
        item.notifyObservers("Dit is een test bericht.");


        //   assertTrue(item.getItemState() instanceof ReadyForTestingState);
        assertEquals("Scrum master: Received message from Test item, Dit is een test bericht.".trim(), outputStreamCaptor.toString().trim());
    }
    @Test
    public void notifyObserversTestPath1_3_5_6(){
        Thread thread = new Thread(this.item);
        thread.customObservers.add(new Developer("Test dev"));
        item.setDiscussionThread(thread);
        item.notifyObservers("DISCUSSION Dit is een test bericht.");
        assertEquals("Test dev: Received message from Test item, Dit is een test bericht.".trim(), outputStreamCaptor.toString().trim());

    }

    @Test
    public void broadcastReadyForTestingTestPath1_2(){


        item.broadcastReadyForTesting("RETURNED Dit is een test bericht.");

        assertEquals("Scrum master: Received message from Test item, Dit is een test bericht.".trim(), outputStreamCaptor.toString().trim());
    }
    @Test
    public void broadcastReadyForTestingTestPath1_3_5(){
        board.addUser("Tester1",new Tester("Tester1"));

        item.broadcastReadyForTesting("Dit is een test bericht.");

        assertEquals("Tester1: Received message from Test item, Dit is een test bericht.".trim(), outputStreamCaptor.toString().trim());
    }

    @Test
    public void broadcastReadyForTestingTestPath1_3_4(){
    // Dit is een pad dat alleen in theorie uitgevoerd kan worden.
        item.broadcastReadyForTesting("Dit is een test bericht.");

        assertEquals("".trim(), outputStreamCaptor.toString().trim());
    }

    @Test
    public void broadcastToDoTestPath1_2(){
    ;
        item.broadcastToDo("Dit is een test bericht.");

        assertEquals("Scrum master: Received message from Test item, Dit is een test bericht.".trim(), outputStreamCaptor.toString().trim());
    }
    @Test
    public void broadcastToDoTestPath1_3(){
        // Dit is een pad dat alleen in theorie uitgevoerd kan worden.
        board.getUsers().remove("Scrum master");
        item.broadcastToDo("Dit is een test bericht.");

        assertEquals("".trim(), outputStreamCaptor.toString().trim());
    }

    @Test
    public void broadcastDiscussionTestPath1(){
        Thread thread = new Thread(this.item);
        thread.customObservers.add(new Developer("Test dev"));
        item.setDiscussionThread(thread);
        item.broadcastDiscussion("DISCUSSION Dit is een test bericht.");
        assertEquals("Test dev: Received message from Test item, Dit is een test bericht.".trim(), outputStreamCaptor.toString().trim());
    }


    @Test
    public void TesterShouldReceiveNotificationWhenItemIsSetToReadyForTesting(){
        board.addUser("Tester", new Tester("Tester"));


        item.doing();
        item.readyForTesting();


        assertTrue(item.getItemState() instanceof ReadyForTestingState);
        assertEquals("Tester: Received message from Test item, is ready for testing.".trim(), outputStreamCaptor.toString().trim());
    }
    @Test
    public void ShouldGetErrorMessageWhenItemMovesToInvalidState(){

        item.doing();
        item.readyForTesting();
        item.testing();
        item.done();

        assertTrue(item.getItemState() instanceof TestingState);
        assertEquals("Can't proceed to done proceed to tested first".trim(), outputStreamCaptor.toString().trim());
    }
}